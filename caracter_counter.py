lower_cas_alfpabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x",
"y","z"]
f = open("input.txt","r")
content = f.read()
f.close()
seq = {}

for char in content:
    if char in seq:
        if char in lower_cas_alfpabet:
          seq[char] = seq[char] + 1

    elif char in lower_cas_alfpabet: 

        seq[char] = 0

n = open("output.txt","a")      
for element in seq :
    n.write(f"{element} : {seq[element]} \n")
n.close()